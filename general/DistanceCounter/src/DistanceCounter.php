<?php

namespace src;

use Exception;

/**
 * Class DistanceCounter
 * @package src\DistanceCounter
 */
class DistanceCounter {

	/**
	 * @var array
	 */
	private $pointA = [];
	/**
	 * @var array
	 */
	private $pointB = [];

	/**
	 * @var array
	 */
	private $exceptionsList = [];

	/**
	 * dane dwóch punktow przekazujemy do konstruktora
	 *
	 * DistanceCounter constructor.
	 * @param $pointA
	 * @param $pointB
	 * @throws Exception
	 */
	public function __construct(string $pointA, string $pointB) {

		try {

			$this->areNotIdentical($pointA, $pointB);

			$this->pointA = $this->createPoint($pointA);
			$this->pointB = $this->createPoint($pointB);

		}
		catch (Exception $exception) {

			$this->exceptionsList[$exception->getCode()] = $exception->getMessage();		}

	}


	/**
	 * @return array
	 */
	public function getPointA(): array {
		return $this->pointA;
	}

	/**
	 * @return array
	 */
	public function getPointB(): array {
		return $this->pointB;
	}


	/**
	 * Tablica ze zlapanymi wyjatkami (kod => wiadomosc)
	 *
	 * @return array
	 */
	public function getExceptionsList(): array {
		return $this->exceptionsList;
	}

	private function areNotIdentical(string $string1, string $string2): bool {
		if (trim($string1) !== trim($string2)) {
			return true;
		}
		else {
			throw new Exception("The values cannot be the same", 1000);
		}
	}

	/**
	 *
	 * Na podstawie stringa przekazanego z formularza tworzy tablice z danymi
	 *
	 * @param string $string
	 * @return array
	 *
	 * @throws Exception
	 */
	private function createArrayFromCoordinates(string $string): array {


		if (strpos($string, ',') === false) {
			throw new Exception('Comma separator missing', 1001);
		}

		return array_map('trim', explode(",", $string));

	}

	/**
	 * Sprawdzanie poprawnosci przekazanych danych
	 *
	 * @param array $point
	 * @return array
	 * @throws Exception
	 */
	private function validateCoordinates(array $point): array {

		$resultLat = preg_match(
			'/^([+\-])?(?:90(?:(?:\.0{1,6})?)|(?:[0-9]|[1-8][0-9])(?:(?:\.[0-9]{1,6})?))$/',
			$point['lat']);

		$resultLng = preg_match(
			'/^-?((([1]?[0-7][0-9]|[1-9]?[0-9])\.{1}\d{1,6}$)|[1]?[1-8][0]\.{1}0{1,6}$)/',
			$point['lng']);


		switch (true) {
			case $resultLat == 0:
				throw new Exception('Wrong coordinate value: ' . $point['lat'], 1002);
				break;
			case $resultLng == 0:
				throw new Exception('Wrong coordinate value: ' . $point['lng'], 1002);
				break;
			default:
				return $point;
		}

	}

	/**
	 *
	 * Tworzenie tablicy asocjacyjnej, z kluczami 'lat' (latitude) oraz 'lng' longitude
	 *
	 * @param array $coordinates
	 * @return array|false
	 */
	private function addKeysToArray(array $coordinates): array {

		return array_combine(['lat', 'lng'], $coordinates);

	}

	/**
	 * Funkcja tworząca punkt (implementacja innych funkcji prywatnych), utworzona dla zachowania zasady DRY
	 *
	 * @param string $point
	 * @return array
	 * @throws Exception
	 */
	private function createPoint(string $point): array {

		return $this->validateCoordinates(
			$this->addKeysToArray(
				($this->createArrayFromCoordinates($point))
			)
		);

	}

	/**
	 * promień ziemi jest podany oraz zwracany domyslnie w metrach,
	 * w razie potrzeby mozna przekazać wartosc w innej jednostce miary
	 * lub rozszerzyc klase o mozliwosc wyboru jednostki
	 *
	 * @param float $earthRadius
	 * @return float
	 */
	public function countGreatCircleDistance(float $earthRadius = 6371000): float {

		$latFrom = deg2rad($this->pointA['lat']);
		$lonFrom = deg2rad($this->pointA['lng']);
		$latTo   = deg2rad($this->pointB['lat']);
		$lonTo   = deg2rad($this->pointB['lng']);

		$lonDelta = $lonTo - $lonFrom;

		$a = pow(cos($latTo) * sin($lonDelta), 2) +
			pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
		$b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);

		$angle = atan2(sqrt($a), $b);

		return round((($angle * $earthRadius)) / 1000, 2);

	}

	/**
	 * zamiana wartości float na stringa, w celu użycia we front-endzie
	 *
	 * @return string
	 */
	public function greatCircleDistanceAsString(): string {
		return str_replace('.', 'km ', $this->countGreatCircleDistance()) . 'm';
	}

}

