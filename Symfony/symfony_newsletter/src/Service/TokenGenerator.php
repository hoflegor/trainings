<?php


namespace App\Service;


use Symfony\Component\HttpFoundation\Request;

/**
 * Class TokenGenerator
 * @package App\Service
 */
class TokenGenerator {

	/**
	 * @param Request $request
	 * @return string
	 */
	public function create(Request $request): string
	{
		return $request->getSession()->getId() . time();
	}

}