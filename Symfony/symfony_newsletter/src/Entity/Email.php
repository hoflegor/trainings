<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="email")
 */
class Email
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", unique=true, length=254)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $userToken;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getUserToken(): ?string
    {
        return $this->userToken;
    }

public function setUserToken(string $userToken): self
    {
        $this->userToken = $userToken;

        return $this;
    }
}
