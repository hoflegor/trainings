<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Parsedown;


class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {

        $path = $this->get('kernel')
                ->getProjectDir() . '/README.md';

        $Parsedown = new Parsedown();
        $content = $Parsedown->text(file_get_contents($path));

        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'content' => $content
        ]);

    }
}
