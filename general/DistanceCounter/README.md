
# Zadanie 

Aplikacja ma obliczać odległość między dwoma punktami geograficznymi z następującymi założeniami:

1. Aplikacja wyświetla formularz umożliwiający podanie współrzędnych dwóch punktów geograficznych.
2. Jako wynik działania aplikacji zwracana jest odległość w metrach oraz kilometrach pomiędzy tymi dwoma punktami.

----
* Zadanie wykonane w PHP 7.3
 
* Plik `index.php` w folderze `./public`.

* Testy odpalamy w folderze `./tests`

    `$ ./vendor/bin/phpunit tests/src/DistanceCounterTest.php`

* Komentarze w PhpDoc

* Przykładowy wynik dla wartości:
    * A (-90.0, -180.0)
    * B (-77.284382, 1.123456)<br>

    to 1413km 91m
    
        The distance between points:
 
        A (-90.0, -180.0)
        B (-77.284382, 1.123456)
 
        is about:
 
        1413km 91m
 

---- 