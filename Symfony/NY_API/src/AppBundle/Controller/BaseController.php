<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Author;
use AppBundle\Entity\Book;
use AppBundle\Repository\AuthorRepository;
use AppBundle\Repository\BookRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\QueryBuilder;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Traversable;

class BaseController extends Controller
{

    /**
     * @return AuthorRepository
     */
    protected function getAuthorRepository(): AuthorRepository
    {
        return $this->getDoctrine()
            ->getRepository('AppBundle:Author');
    }

    /**
     * @return BookRepository
     */
    protected function getBookRepository(): BookRepository
    {
        return $this->getDoctrine()
            ->getRepository('AppBundle:Book');
    }


    /**
     * @return ObjectManager
     */
    protected function getEntityManager(): ObjectManager
    {
        return $this->getDoctrine()
            ->getManager();
    }


    /**
     * @param string $fullName
     * @return string
     */
    protected function convertFullName(string $fullName): string
    {
        return str_replace('+', ' ', $fullName);
    }

    /**
     * @param string $fullName
     * @return array
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    protected function getDataFromNYT(string $fullName): array
    {

        $client = HttpClient::create();

        $response = $client->request('GET',
            'https://api.nytimes.com/svc/books/v3/lists/best-sellers/history.json?author=' .
            $fullName
            . '&api-key=wr1kWsQCY3ZqnRU7GHetqjPGjkFVt12n'
        );

        $content = $response->getContent();
        $content = json_decode($content, true)['results'];

        return $content;

    }

    /**
     * @param string $fullName
     * @return string
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    protected function countBooksByAuthor(string $fullName): string
    {
        return $this->getAuthorRepository()
            ->countBooksByAuthor($fullName);
    }

    /**
     * @return QueryBuilder
     */
    protected function getAuthors()
    {

        return $this->getAuthorRepository()
            ->findAllQueryBuilder();
    }

    /**
     * @param $filters
     * @return QueryBuilder
     */
    protected function getBooks($filters)
    {


        $title = isset ($filters['title']) ? $filters['title'] : null;
        $author = isset ($filters['author']) ? $filters['author'] : null;
        $description = isset ($filters['description']) ? $filters['description'] : null;
        $reviews = isset ($filters['reviews']) ? $filters['reviews'] : null;

        return $this->getBookRepository()
            ->findAllQueryBuilder($title, $author, $description, $reviews);
    }


    /**
     * @param array $data
     * @param int $count
     * @param string $fullName
     * @return Response
     * @throws DBALException
     */
    protected function parseImportData(array $data, int $count, string $fullName): Response
    {

        $importedAmount = 0;

        if (count($data) - $count > 0) {

            $importedAmount = $this->addData(
                $data,
                $importedAmount);

        }

        $totalAmount = $this->getBookRepository()
            ->countBookByAuthor($fullName);

        $data = $this->getImportedData($importedAmount, $totalAmount);

        return $this->createApiResponse($data);

    }

    /**
     * @param int $importedAmount
     * @param int $totalAmount
     * @return array
     */
    protected function getImportedData(int $importedAmount, int $totalAmount): array
    {
        return ['import' => [
            'imported' => $importedAmount,
            'total' => $totalAmount
        ]];
    }

    /**
     * @param string $jsonTitle
     * @param int $currentPage
     * @param int $numerOfbPages
     * @param int $numberOfResults
     * @param int $maxPerPage
     * @param Traversable $currentPageResults
     * @param array $authors
     * @return array
     */
    protected function getJsonData(
        string $jsonTitle,
        int $currentPage,
        int $numerOfbPages,
        int $numberOfResults,
        int $maxPerPage,
        Traversable $currentPageResults,
        array $authors
    ): array
    {
        return [$jsonTitle => [
            'currentPage' => $currentPage,
            'totalPages' => $numerOfbPages,
            'totalItems' => $numberOfResults,
            'itemsPerPage' => $maxPerPage,
            'itemsOnCurrentPage' => count($currentPageResults),
            'list' => $authors,
        ]
        ];
    }

    /**
     * @param Pagerfanta $pagerfanta
     * @return array
     * @throws DBALException
     */
    protected function createAuthorsData(Pagerfanta $pagerfanta): array
    {

        $authors = [];

        foreach ($pagerfanta->getCurrentPageResults() as $author) {
            $fullName = $author->getFullName();
            $authors[] = [
                'fullName' => $fullName,
                'booksAmount' => $this->countBooks($fullName),
            ];
        }

        return $authors;

    }

    /**
     * @param Pagerfanta $pagerfanta
     * @return array
     */
    protected function createBooksData(Pagerfanta $pagerfanta): array
    {

        $books = [];

        foreach ($pagerfanta->getCurrentPageResults() as $book) {
            $books[] = [
                'title' => $book->getTitle(),
                'author' => $book->getAuthor()->getFullName(),
                'description' => $book->getDescription(),
                'reviewsAmount' => $book->getReviews(),

            ];
        }

        return $books;

    }


    /**
     * @param array $data
     * @param int $importedAmount
     * @return int
     */
    protected function addData(array $data, int $importedAmount): int
    {

        foreach ($data as $item) {

            $fullName = $item['author'];

            $authorEntity = $this->getAuthorRepository()
                ->findOneBy([
                    'full_name' => $fullName
                ]);

            switch ($authorEntity) {
                case !null:
                    $title = $item['title'];

                    $bookEntity = $this->getBookRepository()
                        ->findOneBy([
                            'title' => $title,
                            'author' => $authorEntity
                        ]);

                    if ($bookEntity == null) {

                        $book = new Book();
                        $book->setTitle($title);
                        $book->setDescription($item['description']);
                        $book->setReviews(count(array_filter($item['reviews'][0])));
                        $book->setAuthor($authorEntity);

                        $importedAmount++;

                        $this->getEntityManager()->persist($book);
                        $this->getEntityManager()->flush();
                    }
                    break;

                default:
                    $author = new Author();
                    $author->setFullName($fullName);
                    $this->getEntityManager()->persist($author);

                    $book = new Book();
                    $book->setTitle($item['title']);
                    $book->setDescription($item['description']);
                    $book->setReviews(count(array_filter($item['reviews'][0])));
                    $book->setAuthor($author);

                    $importedAmount++;

                    $this->getEntityManager()->persist($book);
                    $this->getEntityManager()->flush();

            }

        }


        return $importedAmount;

    }

    protected function getPagerfanta(Request $request, QueryBuilder $queryBuilder): Pagerfanta
    {

        $page = $request->query->get('page', 1);

        $adapter = new DoctrineORMAdapter($queryBuilder);
        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta->setMaxPerPage(10);
        $pagerfanta->setCurrentPage($page);

        return $pagerfanta;

    }


    /**
     * @param string $fullName
     * @return int
     * @throws DBALException
     */
    protected function countBooks(string $fullName): int
    {
        return $this->getBookRepository()
            ->countBookByAuthor($fullName);
    }

    protected function createApiResponse(array $data, int $statusCode = 200): Response
    {

        $json = json_encode($data);

        return new Response($json, $statusCode, array(
            'Content-Type' => 'application/json'
        ));
    }

}
