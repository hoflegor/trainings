<?php
/**
 * Date: 08/08/2018
 * Time: 16:20
 * @author Artur Bartczak <artur.bartczak@code4.pl>
 */

namespace Proexe\BookingApp\Utilities;


use Proexe\BookingApp\Offices\Models\OfficeModel;

class DistanceCalculator {

    /**
     * @param array  $from
     * @param array  $to
     * @param string $unit - m, km
     *
     * @return mixed
     */
    public function calculate($from, $to, $unit = 'm') {

        $earthRadiusInMeters = 6371000;

        // By using such constructed array you can easily convert the distance,
        //e.g. into miles: $earthRadiusInMeters * 0.00062137
        $earthRadius = ['m'  => $earthRadiusInMeters,
                        'km' => $earthRadiusInMeters / 1000];


        $latFrom = deg2rad($from[0]);
        $lonFrom = deg2rad($from[1]);
        $latTo   = deg2rad($to[0]);
        $lonTo   = deg2rad($to[1]);

        $lonDelta = $lonTo - $lonFrom;

        $a = pow(cos($latTo) * sin($lonDelta), 2) +
            pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
        $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);

        $angle = atan2(sqrt($a), $b);

        return round((($angle * $earthRadius[$unit])));

    }

    /**
     * @param array $from
     * @param array $offices
     *
     * @return array
     */
    public function findClosestOffice($from, $offices) {

        $closestOffice = ['name'     => null,
                          'distance' => null,
        ];

        foreach ($offices as $office) {

            $distance = $this->calculate([$from[0], $from[1]], [$office['lat'], $office['lng']], 'km');

            if (!isset($closestOffice['distance']) || $closestOffice['distance'] > $distance) {
                $closestOffice['name']     = $office['name'];
                $closestOffice['distance'] = $distance;
            }

        }

        return $closestOffice;

    }

}

// pure SQL solution for finding closest office

/*SELECT name,
       (6371 * acos(
               cos(radians(14.12232322))
               * cos(radians(lat))
               * cos(radians(lng) - radians(8.12232322))
               + sin(radians(14.12232322))
               * sin(radians(lat))
           )) as distance
from offices
ORDER BY distance
LIMIT 1;*/
