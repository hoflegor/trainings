<?php


namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\QueryBuilder;

class AuthorRepository extends EntityRepository
{

    /**
     * @param string $fullName
     * @return string
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function countBooksByAuthor(string $fullName): string
    {

        $query = $this->getEntityManager()->createQueryBuilder()
            ->select('count(author.id)')
            ->where('author.full_name LIKE :fullNme')
            ->setParameter('fullNme', '%' . $fullName . '%')
            ->from('AppBundle:Author', 'author');

        return $query->getQuery()->getSingleScalarResult();
    }

    /**
     * @return QueryBuilder
     */
    public function findAllQueryBuilder(): QueryBuilder
    {
        return $this->createQueryBuilder('author');
    }

}