<?php

namespace App\Controller;

use App\Entity\Email;
use App\Service\TokenGenerator;
use Doctrine\ORM\EntityManagerInterface as EntityManager;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class AddressController extends AbstractController {

	/**
	 * @var EntityManager
	 */
	private $em;
	/**
	 * @var TokenGenerator
	 */
	private $tokenGenerator;

	public function __construct(EntityManager $entityManager, TokenGenerator $tokenGenerator)
	{
		$this->em             = $entityManager;
		$this->tokenGenerator = $tokenGenerator;
	}

	/**
	 * @Route("/newEmail", name="newEmail", methods={"POST", "GET"})
	 * @param Request $request
	 * @return Response
	 */
	public function newEmail(Request $request)
	{
		if ($request->isMethod('post') &&
			$request->get('userEmail')) {

			$email = $this->em->getRepository('App:Email')->findOneBy(
				['address' => $request->get('userEmail')]
			);

			dump($email);

			switch ($email) {
				case null:
					$email = new Email();
					$email->setAddress($request->get('userEmail'));

					$this->em->persist($email);
					$this->em->flush();

					return ($this->forward('App\Controller\AddressController::successAdd',
						[
							'email'   => $email,
							'request' => $request,
						]));
				default:
					return ($this->forward('App\Controller\AddressController::userExist',
						[
							'email'   => $email,
							'request' => $request,
							'token'   => $email->getUserToken(),
						]));

			}


		}

		return $this->render('address/newEmail.html.twig', [
			'controller_name' => 'AddressController/newEmail',
			'message'         => '',
		]);
	}

	/**
	 * @Route("/successAdd/{email}/{token}", name="successAdd")
	 * @param Email   $email
	 * @param Request $request
	 * @return RedirectResponse|Response
	 */
	public function successAdd(Email $email, Request $request)
	{

		$session = $request->getSession();

		$email = $this->em->getRepository('App:Email')->findOneBy([
			'address' => $email->getAddress(),
		]);

		$token = $this->tokenGenerator->create($request);

		$email->setUserToken($token);
		$this->em->flush();

		$session->set('token', $token);

		return $this->render('address/successAdd.html.twig', [
			'controller_name' => "AddressController/successAdd",
		]);

	}

	/**
	 * @Route("/userExist/{email}", name="userExist")
	 * @param Email   $email
	 * @param Request $request
	 * @return Response
	 */
	public function userExist(Email $email, Request $request)
	{

		if ($request->isMethod('post')) {

			$email->setUserToken($this->tokenGenerator->create($request));

			$this->em->flush();

			return $this->render('address/userExist.html.twig', [
				'controller_name' => 'AddressController/userExist',
				'method'          => 'post',
				'token'           => $email->getUserToken(),
			]);
		}

		return $this->render('address/userExist.html.twig', [
			'controller_name' => 'AddressController/userExist',
			'method'          => 'get',
			'token'           => $request->getSession()->get('token'),
		]);

	}

	/**
	 * @Route("/deleteEmail/{userToken}", name="deleteEmail")
	 * @param Request $request
	 * @param         $userToken
	 */
	public function deleteEmail(Request $request, $userToken)
	{

		dump($userToken);

		$email = $this->em->getRepository('App:Email')->findOneBy([
			'userToken' => $userToken,
		]);


		$this->em->remove($email);
		$this->em->flush();

		return $this->render('address/deleteEmail.html.twig', [
			'controller_name' => 'AddressController/deleteEmail',
			'userMail'        => $email->getAddress(),
		]);

	}

}
