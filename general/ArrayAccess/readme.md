* Stwórz klasę implementującą ArrayAccess interface umożliwiającą dostęp (odczyt i zapis) do jednego z 32 bitów, dla liczby typu int przekazanej w konstruktorze.

* 

* Kod sprawdzający działanie w pliku `index.php`.<br>
Odpalamy komendą: <br>
`php index.php arg1 arg2 arg3 arg4` - opis poszczególnych argumentów w pliku<br><br>
Np.:<br>
`php index.php php index.php 21474821 23 1` <br>
Wypisze:

      Array
      (
        [0] => 1

        [1] => 0

        [2] => 1

        [3] => 0

        [4] => 0

        [5] => 0

        [6] => 1

        [7] => 1

        [8] => 1

        [9] => 1

        [10] => 0

        [11] => 1

        [12] => 0

        [13] => 1

        [14] => 1

        [15] => 1

        [16] => 0

        [17] => 0

        [18] => 0

        [19] => 0

        [20] => 0

        [21] => 0

        [22] => 1

        [23] => 0

        [24] => 1

      )
    
        offsetExist (bool):
        1

        offsetGet:
        0

        offsetGet - changed to:
        1

    