<?php

require_once ('AwesomeArrayClass.php');

if (count($argv)!=3) {

	die(print_r("Two int values must be given (rows & columns) !!!!\n"));

}
else{
	$myArray = new AwesomeArrayClass($argv[1],$argv[2]);

	echo "\e[4m\nARRAY:\n\e[0m";
	print_r($myArray->getArray());

	echo "\e[4m\nARRAYS VALUES:\n\e[0m";
	$myArray->printArraysValues();

	echo "\e[4m\nSNEAK VALUES:\n\e[0m";
	$myArray->printSneakValuesFromArray();
	echo "\n";
}