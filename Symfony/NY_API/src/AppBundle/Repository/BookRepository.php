<?php


namespace AppBundle\Repository;

use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

class BookRepository extends EntityRepository
{

    /**
     * @param string $fullName
     * @return int
     * @throws DBALException
     */
    public function countBookByAuthor(string $fullName): int
    {

        $conn = $this->getEntityManager()->getConnection();

        $query = "SELECT count(*) as amount FROM book b
                    JOIN author a  ON b.author_id = a.id
                    where a.full_name LIKE :author";

        $stmt = $conn->prepare($query);

        $stmt->execute(['author' => '%' . $fullName . '%']);;

        return (int)$stmt->fetch()['amount'];

    }

    /**
     * @param $title
     * @param $author
     * @param $description
     * @param $reviews
     * @return QueryBuilder
     */
    public function findAllQueryBuilder($title, $author, $description, $reviews): QueryBuilder
    {

        $queryBuilder = $this->createQueryBuilder('book');

        if ($title) {
            $queryBuilder
                ->andWhere('book.title LIKE :title ')
                ->setParameter('title', '%' . $title . '%');
        }

        if ($author) {
            $queryBuilder
                ->join('book.author', 'a')
                ->where('a.full_name LIKE :fullNName')
                ->setParameter('fullNName', '%' . $author . '%');

        }

        if ($description) {
            $queryBuilder
                ->andWhere('book.description LIKE :description ')
                ->setParameter('description', '%' . $description . '%');
        }

        if ($reviews) {
            $queryBuilder
                ->andWhere('book.reviews >= :reviews ')
                ->setParameter('reviews', $reviews);
        }

        return $queryBuilder;

    }

}