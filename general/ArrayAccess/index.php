<?php

require_once(__DIR__ . '/ArrayAccessClass.php');

//Tworzenie nowego obiekty, z liczby przekazanej jako pierwszy parametr w komendzie,
//jeśli udało się otworzyć wypisuje stworzony $container
$myObj = new ArrayAccessClass($argv[1]);

//Sprawdzanie czy offset istnieje, zwraca true/false - drugi parametr
echo "\e[4m\noffsetExist (bool):\n\e[0m";
echo($myObj->offsetExists($argv[2]));

//Getter wartości offseta - drugi parametr
echo "\n\e[4m\noffsetGet:\n\e[0m";
print_r($myObj->offsetGet($argv[2]));

//Zmienianie wartości
$myObj->offsetSet($argv[2], $argv[3]);

//Sprawdzanie czy udało się zmienić wartość
echo "\n\e[4m\noffsetGet - changed to:\n\e[0m";
print_r($myObj->offsetGet($argv[2]));
echo "\n";
