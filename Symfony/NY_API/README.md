ny_api
======
### Założenia treningu:
##### Stworzenie API do przeglądania książek autorów opublikowanych na liście bestsellerów New York Timesa.

1. Aplikacja umożliwia import książek oraz ich autorów.
    - przy imporcie można wybrać tylko jednego autora.
    - importy można przeprowadzać wielokrotnie

2. API udostępnia paginowaną listę zaimportowanych książek, w formacie JSON, z
możliwością filtrowania ich po:

    - frazie szukającej w tytule (title) i opisie (description) książki
    - autorze (author)
    - minimalnej ilości recenzji (reviews)

3. API powinno udostępnić paginowaną listę autorów.

Przy imporcie wykorzystałem API New York Times'a:

https://developer.nytimes.com/docs/books-product/1/routes/lists/bestsellers/history.json/get <br>

Działanie aplikacji:
==

##### Aplikacja napisana w `Symfony 3.4.36`/`PHP 7.3.9`,<br>db:`MariaDB ver. 5.5.5-10.1.38`

#### <u>Start</u>:

* Instalacja bibliotek: `$ composer install`

* Konfiguracja bazy danych dostępna w pliku: `(...)/app/config/parameters.yml`

* Start wbudowanego serwer `$ php bin/console server:start`

* Utworzenie bazy: `$ ./bin/console doctrine:database:create`

* Migracja: `$ ./bin/console doctrine:migrations:migrate`

* Na wszelki wypadek można odpalić komendę `$ ./bin/console cache:clear`

* Skonwertowany do HTML plik README.md jest dostępny na stronie domowej projektu:
<a href>localhost:8000</a>
(lub innym zdefiniowanym podczas startu serwera)

* Przykładowe dane załączone w folderze [./.dump](./.dump)

#### <u>Testy</u>:

Testy odpalamy komendą:

`$ ./vendor/bin/simple-phpunit`

#### <u>Import</u>:

* ##### URI:
    `http://localhost:8000/api/import/Imie+Nazwisko`
    Np.: http://localhost:8000/api/import/Stephen+King
* ##### Response:
    Ilość zaimportowanych książek oraz ilość wszystkich książek autora w bazie danych, np.:

      {
        import: {
            imported: 4,
            total: 20
        }
      }

* Bestsellery są pobierane z listy: `/history.json?author?author=name+surname`. Założyłem, iż lista może być uaktualniana tylko poprzez dodanie nowej pozycji na końcu listy.

* Podczas importu aplikacja sprawdza czy ilość pozycji (`results`) zgadza się z ilością zapisaną w bazie danych - jeśli nie dodaje brakujące pozycje/autorów.

* Autorem może być jedna osoba lub kilka. Książka pod tym samym tytułem może pojawić się kilka razy, jeśli zawiera innych autorów, np.:

      {
        "title": "AMERICAN VAMPIRE, VOL. 1",
        "author": "Scott Snyder and Stephen King"
      }

      {
        "title": "AMERICAN VAMPIRE, VOL. 1",
        "author": "Scott Snyder, Stephen King and Rafael Albuquereque"
      }

    Zatem w bazie danych autor (`fullname`) jest unikalny, a tytuł książki (`title`) może pojawić się kilka razy.

* <i>Response</i> zwraca :
    * liczbę książek zaimportowanych przy ostatnim żądaniu
    * Liczbę wszystkich książek autora   

#### <u>Authors</u>:

http://localhost:8000/api/authors

    *domyślnie 'page=1'

    * Paginacja
    'http://localhost:8000/api/authors?page=2'

* <i>Response</i> zwraca :
    * aktualną stronę
    * Ilość wszystkich stron
    * liczbę wyników na liście
    * maksymalną liczbę wyników na stronę
    * liczbę wyników na aktualnej stronie
    * listę książek:
        * tytuł
        * autora
        * opis
        * liczbę recenzji


#### <u>Books</u>:

http://localhost:8000/api/books

    * Domyślnie 'page=1'

    * Paginacja
    http://localhost:8000/api/books?page=2

#### <u>Filtrowanie książek</u>

Filtrowanie poprzez dodanie parametrów:

    * Tytuły:
    ?title=jakies+dane

    *Autor:
    ?author=jakies+dane

    * Opis:
    ?description=jakies+dane

    * Recenzje
    ?reviews=2


    Parametry/paginację można łączyć, np.:
    
http://localhost:8000/api/books?title=SL&stephen+king&description=now+gr&reviews=2    

* <i>Response</i> zwraca :
    * aktualną stronę
    * Ilość wszystkich stron
    * liczbę wyników na liście
    * maksymalną liczbę wyników na stronę
    * liczbę wyników na aktualnej stronie
    * listę autorów:
        * nazwę autora
        * ilość książek autora

Możliwe jest zwrócenie pustej listy (gdy nie znaleziono wyników).

#### <u>ToDo</u>:

1. Testy

2. API stephen=kin != Stephen	

3. Utworzenie serwisów/kontenerów (Dependency Injection)

4. Importu danych - ulepszenie, implementacja komendy konsolowej/importera danych

5. Walidacja i sanityzacja danych -  zastosowanie formularzy do walidacji GETa (ochrona przed ujemnym 'page'). 

6. Prezentacja danych - ulepszenie tworzenia response, użycie serializera.


---

<a href="http://www.wtfpl.net/">
    <img src="http://www.wtfpl.net/wp-content/uploads/2012/12/wtfpl-badge-4.png" width="80" height="15" alt="WTFPL" />
</a> © 2020 hoflegor - Do What the Fuck You Want to Public License.
