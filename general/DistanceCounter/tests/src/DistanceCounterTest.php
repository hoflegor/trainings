<?php

use PHPUnit\Framework\TestCase;
use src\DistanceCounter;

/**
 * Class DistanceCounterTest
 */
final class DistanceCounterTest extends TestCase {

	/**
	 * @var
	 */
	protected $DistanceCounter;

	/**
	 * @throws Exception
	 */
	private function setUpCorrectData(): void {

		$pointA = "-77.284382, -179.010293";

		$pointB = "-90.0, -180.0";

		$this->DistanceCounter = new DistanceCounter($pointA, $pointB);

	}


	/**
	 * @throws Exception
	 */
	private function setUpSameValues(): void {

		$pointA = "-90.0, -180.0";

		$pointB = "-90.0, -180.0";

		$this->DistanceCounter = new DistanceCounter($pointA, $pointB);

	}

	/**
	 * @throws Exception
	 */
	private function setUpWrongFormat(): void {

		$pointA = "90.12345, -180";

		$pointB = "-20.1234567, 190.1";

		$this->DistanceCounter = new DistanceCounter($pointA, $pointB);

	}

	/**
	 * @throws Exception
	 */
	private function setUpNoComma(): void {

		$pointA = "90.12345 -180";

		$pointB = "-20.1234567 190.1";

		$this->DistanceCounter = new DistanceCounter($pointA, $pointB);

	}

	/**
	 * @throws Exception
	 */
	public function testDistanceCounterConstructorCorrectData(): void {

		$this->setUpCorrectData();

		$expectedPointA = [
			'lat' => -77.284382,
			'lng' => -179.010293,
		];

		$expectedPointB = [
			'lat' => -90.0,
			'lng' => -180.0,
		];

		$this->assertEquals([$expectedPointA, $expectedPointB],
			[$this->DistanceCounter->getPointA(), $this->DistanceCounter->getPointB()]);

	}

	/**
	 * @throws Exception
	 */
	public function testIsGreatCircleDistanceInKmReturnFloat(): void {

		$this->setUpCorrectData();

		$this->assertIsFloat($this->DistanceCounter->countGreatCircleDistance(),
			"testIsGreatCircleDistanceInKmReturnFloat failed",);

	}

	/**
	 * @throws Exception
	 */
	public function testDistanceCounterConstructorsSameValues(): void {

		$this->setUpSameValues();

		$this->assertArrayHasKey('1000', $this->DistanceCounter->getExceptionsList());

	}

	/**
	 * @throws Exception
	 */
	public function testDistanceCounterConstructorNoComma(): void {

		$this->setUpNoComma();

		$this->assertArrayHasKey('1001', $this->DistanceCounter->getExceptionsList());

	}

	/**
	 * @throws Exception
	 */
	public function testDistanceCounterConstructorWrongFormat(): void {

		$this->setUpWrongFormat();

		$this->assertArrayHasKey('1002', $this->DistanceCounter->getExceptionsList());

	}

	/**
	 * @throws Exception
	 */
	public function testGreatCircleDistanceAsString():void {

		$this->setUpCorrectData();

		$expectedResult = '1413km 91m';

		$this->assertEquals($expectedResult, $this->DistanceCounter->greatCircleDistanceAsString());

	}

}
