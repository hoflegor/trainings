<?php


class ArrayAccessClass implements ArrayAccess {

	private $container = [];

	public function __construct(int $number) {

		try {
			$this->container = $this->binaryValidator(decbin($number)); //sprawdzanie czy liczba jest 32 bitowa / ma maks. 32 indeksy
			print_r($this->container);
		}
		catch (Exception $exception) {
			echo "Error: " . $exception->getCode() . "\n" . $exception->getMessage();
			die(); //jeśli jest więcej niż 32 indeksy, nie udało się utworzyć poprawnie obiektu - przerywamy działanie programu
		}

	}

	public function offsetExists($offset) {

		return isset($this->container[$offset]);

	}

	/**
	 * @inheritDoc
	 */
	public function offsetGet($offset) {

		return isset($this->container[$offset]) ? $this->container[$offset] : null;

	}

	/**
	 * @inheritDoc
	 */
	public function offsetSet($offset, $value) {

		if ($this->offsetExists($this->container[$offset])) {
			try {
				$this->container[$offset] = $this->valueValidator($value);
			}
			catch (Exception $exception) {
				echo "Error: " . $exception->getCode() . "\n" . $exception->getMessage();
			}
		}

	}

	/**
	 * @inheritDoc
	 */
	public function offsetUnset($offset) {
		// TODO: Implement offsetUnset() method.
	}

	/**
	 * @param string $decimal
	 * @return array
	 * @throws Exception
	 */
	private function binaryValidator(string $decimal): array {

		if (strlen($decimal) > 32) {
			throw new  Exception(
				"Passed int rejected - number of max allowed indexes for the 32 bit number has been exceeded.  \n", 1000
			);
		}

		return str_split($decimal, 1);

	}

	/**
	 * @param int $value
	 * @return int
	 * @throws Exception
	 */
	private function valueValidator(int $value): int {

		if ($value === 0 || $value === 1) {
			return $value;
		}

		throw new  Exception(
			"Passed val rejected - must be INT: '0' or '1'.  \n", 1001
		);

	}

}

