<?php

class AwesomeArrayClass {

	private $rows;
	private $columns;
	private $array;

	public function __construct(int $rows, int $columns) {
		$this->rows    = $rows;
		$this->columns = $columns;
		$this->makeArray($this->rows, $this->columns);
	}

	public function getArray(): array {
		return $this->array;
	}

	private function makeArray($rows, $columns): void {

		$array = [];

		for ($i = 0; $i < $rows; $i++) {
			for ($j = 0; $j < $columns; $j++) {
				$array[$i][] = rand(1, 100);
			}
		}

		$this->array = $array;

	}

	public function printArraysValues(): void {

		foreach ($this->array as $nestedArray) {
			foreach ($nestedArray as $key => $val) {
				print_r($val);
				echo ' ';
			}
			echo "\n";
		}

	}

	public function printSneakValuesFromArray(): void {

		$arrayToDelete = $this->array;

		while (!empty($arrayToDelete)) {

			foreach (array_shift($arrayToDelete) as $value) {

				print $value . ' ';

			}

			foreach ($arrayToDelete as $nestedArrayKey => $nestedArrayValue) {

				if (count($nestedArrayValue) == 0) {
					continue;
				}

				print array_pop($nestedArrayValue) . ' ';
				$arrayToDelete[$nestedArrayKey] = $nestedArrayValue;

			}

			if ($arrayToDelete != null) {

				foreach (array_reverse(array_pop($arrayToDelete)) as $value) {
					print $value . ' ';

				}

			}

			$index = count($arrayToDelete) - 1;

			while ($index > -1) {

				print array_shift($arrayToDelete[$index]) .' ';
				$index--;

			}

		}

	}

}