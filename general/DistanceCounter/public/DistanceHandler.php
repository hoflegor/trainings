<?php

include_once $_SERVER['DOCUMENT_ROOT'] . '/src/DistanceCounter.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST["pointA"]) && isset($_POST["pointB"])) {

	try {
		$realDistanceCounter = new \src\DistanceCounter($_POST["pointA"], $_POST["pointB"]);
	}
	catch (Exception $e) {
	}

	$exceptions = $realDistanceCounter->getExceptionsList();

	if (!empty($exceptions)) {
		$alert = [];
		$alert['alert']=array_pop($exceptions);
		echo json_encode($alert);
	}
	else{
		echo json_encode(
			[
				'valPointA'   => $realDistanceCounter->getPointA(),
				'valPointB'   => $realDistanceCounter->getPointB(),
				'distance' => $realDistanceCounter->greatCircleDistanceAsString(),
			]
		);
	}

}