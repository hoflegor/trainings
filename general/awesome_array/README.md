## Cel treningu:

##### 1. Stworzenie  metody, która:
 * utworzy  dwuwymiarową tablicę (n-wierszy oraz k-kolumna) i 
 
 * wypełni ją losowymi wartościami int z przedziału od 1 do 100.


* Założenia n-wierszy i k -> kolumn można zmienia podczas wywołania skryptu z CLI

##### 2. Następnie stworzenie drugiej metody, która wypiszę wszystkie wartości startując od indeksu 1,1 idąc w prawo a kończąc w środku tablicy.


Przykład:

> 2 5 6 7
>
> 3 5 8 6
>
> 0 2 2 5
>

Wynik:
>
> 2 5 6 7 6 5 2 2 0 3 5 8

----

## Rozwiązanie:

* Wywołanie skryptu wypisuje:
    
    * stworzoną tablicę
    * wartości tablic zagnieżdżonych
    * wszystkie wartości startując od indeksu 1,1 idąc w prawo a kończąc w środku tablicy

* Uruchomienie skryptu:

    `$ php index.php {wiersze} {kolumny}`     

* Przykład:

    `index.php 4 3`

    Zwraca:


    ARRAY:
    Array
        (
            [0] => Array
                (
                    [0] => 69
                    [1] => 86
                    [2] => 36
                )
 
            [1] => Array
                (
                    [0] => 93
                    [1] => 81
                    [2] => 81
                )
 
            [2] => Array
                (
                    [0] => 73
                    [1] => 33
                    [2] => 47
                )
 
            [3] => Array
                (
                    [0] => 17
                    [1] => 40
                    [2] => 99
                )
 
            )
 
    ARRAYS VALUES:
    69 86 36 
    93 81 81 
    73 33 47 
    17 40 99 
 
    SNEAK VALUES:
    69 86 36 81 47 99 40 17 73 93 81 33