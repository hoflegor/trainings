<?php
/**
 * Date: 09/08/2018
 * Time: 00:16
 * @author Artur Bartczak <artur.bartczak@code4.pl>
 */

namespace Proexe\BookingApp\Utilities;


use Carbon\Carbon;
use Carbon\CarbonInterval;
use Proexe\BookingApp\Offices\Interfaces\ResponseTimeCalculatorInterface;

class ResponseTimeCalculator implements ResponseTimeCalculatorInterface {

    public function calculate($bookingDateTime, $responseDateTime, $officeHours) {

        $bookingDateTime  = Carbon::parse($bookingDateTime);
        $responseDateTime = Carbon::parse($responseDateTime);


        $days    = $bookingDateTime->diffInDays($responseDateTime);
        $hours   = $bookingDateTime->diffInHours($responseDateTime->subDays($days));
        $minutes = $bookingDateTime->diffInMinutes($responseDateTime->subHours($hours));
        $seconds = $bookingDateTime->diffInSeconds($responseDateTime->subMinutes($minutes));

        return CarbonInterval::days($days)->hours($hours)->minutes($minutes)->seconds($seconds)->forHumans();

    }

    //Write your methods here

}
