$(function () {

    $('#distance-form').on('submit', function (e) {

        e.preventDefault();

        $.ajax({
            type: 'POST',
            url: 'DistanceHandler.php',
            data: $('#distance-form').serialize(),
            success: function (data) {

                var obj = jQuery.parseJSON(data);

                if (obj.alert) {
                    $('#alert-message').html(obj.alert +  '. <br>Click <i>More Info</i> button for details.');
                    $('#alert-modal').modal('show');

                }
                else {
                    console.log(obj);
                    $('#pointA-val').html(obj.valPointA.lat + ', ' + obj.valPointA.lng);
                    $('#pointB-val').html(obj.valPointB.lat + ', ' + obj.valPointB.lng);
                    $('#distance-val').html(obj.distance);

                    $('#distance-modal').modal('show');
                }


            },
            error: function (data) {
                alert('An error occurred. See Inspect -> console for more information');
                console.log(data);
            },

        });

    });

});