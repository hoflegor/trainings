# Symfony_newsletter

> Powtórka podstaw symfony (v4.4)
>
> ~~out of date/deprecated~~ - out of date/deprecated in symfony 4.4
>
>> work-in-progress

## 1. Przygotowanie projektu

#### Zadanie 1

Utwórz nowy projekt o nazwie **symfony_newsletter**

~~1.Dodaj nowego bundla o nazwie CodersLabBundle, korzystając z komend konsolowych.~~

~~2.Usuń domyślnego bundla o nazwie AppBundle. Procedura składa się z 3 kroków.~~
    
## 2. Routing i kontrolery

#### Zadanie 1 – generowanie kontrolerów

Stwórz kontroler o nazwie Address, którego zadaniem będzie obsługa formularza zbierającego adresy email
od użytkowników odwiedzających stronę. Dodaj akcje ze standardowym routingiem.

Nazwy akcji to:
* newEmail
* successAdd
* userExist
* deleteEmail

#### Zadanie 2 - akcja newEmail

Zadaniem akcji jest obsługa danych wysłanych metodą POST.
Docelowo będą te dane zapisywane w bazie danych ale na tym etapie tworzenia
systemu wyświetlimy je na stronie.

1. Akcja ma wyświetlić prosty formularz HTML przesyłający dane metodą POST 
   do tej samej akcji, która go wyświetliła. 
   Formularz jest wyświetlany z pomocą obiektu typu Response.
2. Pole formularza do wpisania adresu powinno posiadać atrybut name="userEmail"
3. Jeśli zostaną wysłane dane to z akcji zwracany jest obiekt typu Response,który
   zamiast formularza zawiera informację do wyświetlenia: "Dane zostały wysłane"


#### Zadanie 3 - akcja successAdd

Zadaniem tej akcji jest utworzenie sesji, w której zapisany zostanie niepowtarzalny 
identyfikatora użytkownika tzw. token. Jeśli token istnieje w sesji to po ponownym 
wejściu na stronę newEmail zostajemy przekierowani do akcji userExist.

``` Token w późniejszym etapie zostanie zapisany w bazie danych na potrzeby identyfikacji ```
    
1. W celu wygenerowania tokena należy skorzystać z połączenia dwóch stringów: 
   unikalnego id sesji oraz znacznika czasu UNIX (funkcja time() )
2. Token zapisujemy w sesji pod kluczem "userToken"
3. Akcja zwraca obiekt Response z komunikatem „Adres email został dodany”
4. Modyfikujemy akcję newEmail w taki sposób aby sprawdzała czy token został przydzielony
5. Jeśli został to należy ustawić przekierowanie do akcji userExist
6. Zamieniamy również obiekt Response na przekierowanie do akcji successAdd 

## 3. Widok i Twig

#### Zadanie 1 - widok dla akcji newEmail

Zmieniamy zwracany przez akcję obiekt typu Respons na widok twiga zawierający formularz HTML.

#### Zadanie 2 - widok dla akcji successAdd

Zmieniamy zwracany przez akcję obiekt typu Respons na widok zawierający komunikat "Adres email został dodany"

#### Zadanie 3 – widok dla akcji userExist

Akcja ta w przypadku wejścia metodą GET powinna wyświetlać formularz edycji adresu email. W przypadku wysłania danych metodą POST powinna uaktualnić adres (narazie fikcyjnie) i zamiast formularza edycji wyświetlić komunikat „Adres został uaktualniony”.

#### Zadanie 4 - link do usunięcia adresu

Tuż obok formularza edycji w akcji userExist, dodajemy link do usunięcia adresu i sesji użytkownika. Link prowadzi do akcji deleteEmail przyjmującej jeden parametr na wejściu czyli userToken.
Link powinien posiadać klasę class="btn". Akcja po przyjęciu danych z tokenem użytkownika usuwa klucz w sesji oraz prezentuje komunikat „Adres email został usunięty”.

## 4. Model i doctrine

#### Zadanie 1

Utworzenie bazy danych dla projektu.
    
1. Utwórz model i bazę danych zawierający encję służącą gromadzeniu adresów.
    
2. Encja, której nazwa klasy powinna nazywać się Email, posiada trzy pola:
    * id
    * address
    * userToken.
    
    3. Zmodyfikuj akcję newEmail aby poprawnie zapisywała adres w wygenerowanej bazie.

#### Zadanie 2

Zmodyfikuj akcję userExist i widok do niej aby uaktualniał odpowiedni wpis w bazie danych.

#### Zadanie 3

Zmodyfikuj akcję successAdd aby oprócz prezentacji komunikatu zapisywała userToken w bazie danych.

#### Zadanie 4

Rozszerz akcję deleteEmail o funkcjonalność usuwania encji użytkownika z bazy danych.

## 5. Formularze

#### Zadanie 1 - zmiana formularza newEmail
Zmodyfikuj akcję i widok newEmail w celu skorzystania z formularzy generowanych przez Symfony.


#### Zadanie 2 - zmiana formularza userExits
Zmodyfikuj akcję i widok tak aby korzystała w formularzy Symfony.

## 6. Walidacja

> W zadaniach możesz posłużyć się obiektem walidatora lub walidacją formularzy.

#### Zadanie 1 
Zmodyfikuj akcję pokazującą formularz dodawania nowego adresu do bazy.
Poddaj adres email walidacji, która sprawdzi jego poprawność.
W uproszczonej wersji może to być chociażby sprawdzenie czy zawiera znak "@".
W bardziej rozbudowanej sprawdzenie adresu jako całości czyli poprawność nazwy użytwkonika, domeny.

#### Zadanie 2
Dodaj walidację również do akcji userExist, której celem jest uaktualnienie adresu 
istniejącego już w bazie danych.

## 7. Security

#### Zadanie 1 - panel administratora
W projekcie stwórz kontroler AdminController z jedną akcją allEmail.
Następnie skonfiguruj Symfony tak, żeby ta akcja była dostępna tylko dla statycznych 
użytkowników wpisanych w pliku konfiguracyjnym.

#### Zadanie 2 - widok do panelu administratora
Akcja ma wyświetlić listę zapisanych adresów email w bazie danych z możliwością ich usunięcia.


---

<a href="http://www.wtfpl.net/">
    <img src="http://www.wtfpl.net/wp-content/uploads/2012/12/wtfpl-badge-4.png" width="80" height="15" alt="WTFPL" />
</a> © 2020 hoflegor - Do What the Fuck You Want to Public License.