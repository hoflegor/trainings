<?php

namespace AppBundle\Controller\Api;

use AppBundle\Controller\BaseController;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class BestsellerController extends BaseController
{

    /**
     * @Route(path="/api/import/{fullName}", methods={"GET"}, name="import")
     * @param string $fullName
     * @return Response
     * @throws ClientExceptionInterface
     * @throws DBALException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function importAction($fullName): Response
    {

        $fullName = $this
            ->convertFullName($fullName);

        $data = $this
            ->getDataFromNYT($fullName);

        $countBooks = $this
            ->countBooksByAuthor($fullName);

        echo($this
			->parseImportData($data, $countBooks, $fullName));

        return $this
            ->parseImportData($data, $countBooks, $fullName);

    }

    /**
     * @Route(path="/api/authors", methods={"GET"}, name="authors")
     * @param Request $request
     * @return Response
     * @throws DBALException
     */
    public function listAuthorsAction(Request $request):Response
    {

        $queryBuilder = $this->getAuthors();

        $pagerfanta = $this->getPagerfanta($request, $queryBuilder);

        $authors = $this->createAuthorsData($pagerfanta);

        $response = $this->createApiResponse(

            $this->getJsonData(
                'authors',
                $pagerfanta->getCurrentPage(),
                $pagerfanta->getNbPages(),
                $pagerfanta->getNbResults(),
                $pagerfanta->getMaxPerPage(),
                $pagerfanta->getCurrentPageResults(),
                $authors
            )

        );

        return $response;

    }

    /**
     * @Route(path="/api/books", methods={"GET"}, name="books")
     * @param Request $request
     * @return Response
     */
    public function booksAction(Request $request):Response
    {

        $filters = $request->query->all();
        $queryBuilder = $this->getBooks($filters);

        $pagerfanta = $this->getPagerfanta($request, $queryBuilder);

        $books = $this->createBooksData($pagerfanta);

        $response = $this->createApiResponse(

            $this->getJsonData(
                'books',
                $pagerfanta->getCurrentPage(),
                $pagerfanta->getNbPages(),
                $pagerfanta->getNbResults(),
                $pagerfanta->getMaxPerPage(),
                $pagerfanta->getCurrentPageResults(),
                $books
            )

        );

        return $response;

    }

}